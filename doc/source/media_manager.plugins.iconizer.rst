media_manager.plugins.iconizer package
======================================

Subpackages
-----------

.. toctree::

    media_manager.plugins.iconizer.userinterfaces
    media_manager.plugins.iconizer.utils

Submodules
----------

media_manager.plugins.iconizer.IconizerPlugin module
----------------------------------------------------

.. automodule:: media_manager.plugins.iconizer.IconizerPlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.iconizer
    :members:
    :undoc-members:
    :show-inheritance:
