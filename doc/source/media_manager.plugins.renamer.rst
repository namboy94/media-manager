media_manager.plugins.renamer package
=====================================

Subpackages
-----------

.. toctree::

    media_manager.plugins.renamer.objects
    media_manager.plugins.renamer.userinterfaces
    media_manager.plugins.renamer.utils

Submodules
----------

media_manager.plugins.renamer.RenamerPlugin module
--------------------------------------------------

.. automodule:: media_manager.plugins.renamer.RenamerPlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.renamer
    :members:
    :undoc-members:
    :show-inheritance:
