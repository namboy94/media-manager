media_manager.plugins.batchdownloadmanager.userinterfaces package
=================================================================

Submodules
----------

media_manager.plugins.batchdownloadmanager.userinterfaces.BatchDownloadManagerCli module
----------------------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.userinterfaces.BatchDownloadManagerCli
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.batchdownloadmanager.userinterfaces.BatchDownloadManagerGUI module
----------------------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.userinterfaces.BatchDownloadManagerGUI
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.batchdownloadmanager.userinterfaces
    :members:
    :undoc-members:
    :show-inheritance:
