media_manager.plugins.common.fileops package
============================================

Submodules
----------

media_manager.plugins.common.fileops.FileMover module
-----------------------------------------------------

.. automodule:: media_manager.plugins.common.fileops.FileMover
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.common.fileops.FileRenamer module
-------------------------------------------------------

.. automodule:: media_manager.plugins.common.fileops.FileRenamer
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.common.fileops
    :members:
    :undoc-members:
    :show-inheritance:
