media_manager.plugins.batchdownloadmanager.downloaders package
==============================================================

Submodules
----------

media_manager.plugins.batchdownloadmanager.downloaders.HexChatPluginDownloader module
-------------------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.downloaders.HexChatPluginDownloader
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.batchdownloadmanager.downloaders.TwistedDownloader module
-------------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.downloaders.TwistedDownloader
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.batchdownloadmanager.downloaders
    :members:
    :undoc-members:
    :show-inheritance:
