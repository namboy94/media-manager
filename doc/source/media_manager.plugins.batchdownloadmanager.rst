media_manager.plugins.batchdownloadmanager package
==================================================

Subpackages
-----------

.. toctree::

    media_manager.plugins.batchdownloadmanager.downloaders
    media_manager.plugins.batchdownloadmanager.searchengines
    media_manager.plugins.batchdownloadmanager.userinterfaces
    media_manager.plugins.batchdownloadmanager.utils

Submodules
----------

media_manager.plugins.batchdownloadmanager.BatchDownloadManagerPlugin module
----------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.BatchDownloadManagerPlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.batchdownloadmanager
    :members:
    :undoc-members:
    :show-inheritance:
