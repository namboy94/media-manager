media_manager.plugins.showmanager package
=========================================

Subpackages
-----------

.. toctree::

    media_manager.plugins.showmanager.userinterfaces

Submodules
----------

media_manager.plugins.showmanager.ShowManagerPlugin module
----------------------------------------------------------

.. automodule:: media_manager.plugins.showmanager.ShowManagerPlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.showmanager
    :members:
    :undoc-members:
    :show-inheritance:
