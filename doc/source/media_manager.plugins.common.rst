media_manager.plugins.common package
====================================

Subpackages
-----------

.. toctree::

    media_manager.plugins.common.calc
    media_manager.plugins.common.fileops
    media_manager.plugins.common.onlinedatagetters

Submodules
----------

media_manager.plugins.common.GenericPlugin module
-------------------------------------------------

.. automodule:: media_manager.plugins.common.GenericPlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.common
    :members:
    :undoc-members:
    :show-inheritance:
