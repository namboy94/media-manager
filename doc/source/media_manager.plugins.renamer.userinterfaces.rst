media_manager.plugins.renamer.userinterfaces package
====================================================

Submodules
----------

media_manager.plugins.renamer.userinterfaces.RenamerCli module
--------------------------------------------------------------

.. automodule:: media_manager.plugins.renamer.userinterfaces.RenamerCli
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.renamer.userinterfaces.RenamerGUI module
--------------------------------------------------------------

.. automodule:: media_manager.plugins.renamer.userinterfaces.RenamerGUI
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.renamer.userinterfaces
    :members:
    :undoc-members:
    :show-inheritance:
