media_manager.external package
==============================

Submodules
----------

media_manager.external.xdccbot module
-------------------------------------

.. automodule:: media_manager.external.xdccbot
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.external
    :members:
    :undoc-members:
    :show-inheritance:
