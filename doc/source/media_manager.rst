media_manager package
=====================

Subpackages
-----------

.. toctree::

    media_manager.cli
    media_manager.external
    media_manager.mainuserinterfaces
    media_manager.plugins
    media_manager.startup

Submodules
----------

media_manager.main module
-------------------------

.. automodule:: media_manager.main
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.metadata module
-----------------------------

.. automodule:: media_manager.metadata
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager
    :members:
    :undoc-members:
    :show-inheritance:
