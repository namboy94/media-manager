media_manager.plugins package
=============================

Subpackages
-----------

.. toctree::

    media_manager.plugins.batchdownloadmanager
    media_manager.plugins.common
    media_manager.plugins.iconizer
    media_manager.plugins.renamer
    media_manager.plugins.showmanager

Submodules
----------

media_manager.plugins.PluginManager module
------------------------------------------

.. automodule:: media_manager.plugins.PluginManager
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins
    :members:
    :undoc-members:
    :show-inheritance:
