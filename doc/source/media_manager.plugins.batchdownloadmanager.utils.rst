media_manager.plugins.batchdownloadmanager.utils package
========================================================

Submodules
----------

media_manager.plugins.batchdownloadmanager.utils.BatchDownloadManager module
----------------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.utils.BatchDownloadManager
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.batchdownloadmanager.utils.ProgressStruct module
----------------------------------------------------------------------

.. automodule:: media_manager.plugins.batchdownloadmanager.utils.ProgressStruct
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.batchdownloadmanager.utils
    :members:
    :undoc-members:
    :show-inheritance:
