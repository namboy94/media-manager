media_manager.plugins.iconizer.utils.iconizers package
======================================================

Submodules
----------

media_manager.plugins.iconizer.utils.iconizers.NautilusNemoIconizer module
--------------------------------------------------------------------------

.. automodule:: media_manager.plugins.iconizer.utils.iconizers.NautilusNemoIconizer
    :members:
    :undoc-members:
    :show-inheritance:

media_manager.plugins.iconizer.utils.iconizers.WindowsIconizer module
---------------------------------------------------------------------

.. automodule:: media_manager.plugins.iconizer.utils.iconizers.WindowsIconizer
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: media_manager.plugins.iconizer.utils.iconizers
    :members:
    :undoc-members:
    :show-inheritance:
